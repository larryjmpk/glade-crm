<?php
namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Role;

class RoleRepository extends EloquentRepository implements RepositoryInterface
{
    public $role;

    public function __construct(Role $role)
    {
        parent::__construct($role);
        $this->role = $role;
    }

    /**
     * get role instance
     */
    public function getModel(){
        return $this->role;
    }
}