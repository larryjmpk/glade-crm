<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    public static $roles = [1=>'super-admin',2=>'admin',3=>'employee',4=>'company',];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=> self::$roles[1],
        ];
    }

    public function role($role){
        return [
            'name'=> self::$roles[$role],
        ];
    }
}
