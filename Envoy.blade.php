@servers(['web' => ' root@167.99.198.136'])

@setup
    $dev_dir = '/var/www/html/crm/dev/glade-crm';
    $main_dir = '/var/www/html/crm/main/glade-crm';
@endsetup

@story('deploy')
    pull_repository
@endstory

@task('pull_repository')
    @if($branch === 'master')
        cd /var/www/html/glade/main/glade-crm
        git pull origin master
    @else
        cd /var/www/html/glade/dev/glade-crm
        git pull origin dev
    @endif

    php artisan migrate

@endtask
