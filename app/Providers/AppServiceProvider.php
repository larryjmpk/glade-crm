<?php

namespace App\Providers;

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SuperAdminController;
use App\Repositories\Interfaces\RepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;

use App\Repositories\RoleRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserController::class)
          ->needs(RepositoryInterface::class)
          ->give(function () {
              return (new UserRepository(new \App\Models\User));
          });

          $this->app->when(EmployeeController::class)
          ->needs(RepositoryInterface::class)
          ->give(function () {
              return (new UserRepository(new \App\Models\User));
          });

          $this->app->when(CompanyController::class)
          ->needs(RepositoryInterface::class)
          ->give(function () {
              return (new CompanyRepository(new \App\Models\Company));
          });

          $this->app->when(RoleController::class)
          ->needs(RepositoryInterface::class)
          ->give(function () {
              return (new RoleRepository(new \App\Models\Role));
          });

          $this->app->when(SuperAdminController::class)
          ->needs(RepositoryInterface::class)
          ->give(function () {
              return (new UserRepository(new \App\Models\User));
          });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
