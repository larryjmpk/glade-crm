

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="bg-light">
        <nav class="navbar navbar-expand-md fixed-to navbar-blue bg-dark">
            <a class="navbar-brand" href="#">Glade</a>
            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
            </button>
    
            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                
                <ul class="navbar-nav mr-auto">

                    @if (Route::has('login'))
                        @auth
                            @auth('company')
                                <li class="nav-item">
                                    <a href="{{ route('company.dashboard') }}" class="nav-link">Company Dashboard</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('users.logout')}}">Logout<span class="sr-only"></span></a>
                                </li>
                            @endauth
                            @if(auth()->user()->isAdmin() || auth()->user()->isSuperAdmin())
                                <li class="nav-item">
                                    <a href="{{ route('admin.dashboard') }}" class="nav-link">Admin Dashboard</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('users.logout')}}">Logout<span class="sr-only"></span></a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('employee.dashboard') }}" class="nav-link">Employee Dashboard</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('users.logout')}}">Logout<span class="sr-only"></span></a>
                                </li>
                            @endif
                        @else
                        <li class="nav-item dropdown pull-right">
                            <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a href="{{ route('login') }}" class="dropdown-item">Employee Log in</a>
                                <a href="{{ route('login') }}" class="dropdown-item">Admin Log in</a>
                                <a href="{{ route('login') }}" class="dropdown-item">Super Admin Log in</a>
                                <a href="{{ route('company.login') }}" class="dropdown-item">Company Login</a>
                            </div>
                        </li>
                        @endauth
                    
                    @endif
                    
                </ul>
            </div>
        </nav>
        <br>
        <main role="main" class="container" id="app">
            <div class="container-fluid pt-100">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>Companies</h3>
                            </div>
                            <div class="card-body">
                                <company-component />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>

