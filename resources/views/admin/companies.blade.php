@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Companies</h3>
        </div>
        <companies-component is-super-admin="{{$is_super}}"/>
    </div>
@endsection