<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin() || $this->user()->isSuperAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        info($this->all());
        return [
            'name'=>'required|string',
            'email' => 'required|unique:companies|string|email',
            'password' => 'required|string',
            'url' => 'required|string',
            'logo_path' => 'nullable|file',
            'user_id'=>'required|exists:users,id'
        ];
    }
}
