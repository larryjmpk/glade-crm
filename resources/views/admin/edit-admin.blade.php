@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Edit Admin</h3>
        </div>
        <div class="card-body">
        <edit-user-component user-type="admin" user-id="{{$id}}" panel-title="Edit Adin" post-action={{route('admin.update-user',['id'=>$id])}}>
        </div>
    </div>
    
@endsection