<?php
namespace App\Repositories\Interfaces;

use App\Models\Role;

interface RoleRepositoryInterface {

    /**
     * create a role
     */
    public function create(array $data);

    /**
     * gel all roles
     */
    public function all();

    /**
     * get a role
     */
    public function get($id);

    /**
     * update a role
     */
    public function update($model, array $data);

    /**
     * delete a role
     */
    public function delete($model);
        
}