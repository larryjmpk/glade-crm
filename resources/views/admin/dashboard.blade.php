@extends('layouts.admin')

@section('content')
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Companies -- {{$companies}}</h5>
          <p class="card-text">Number of companies in the system</p>
          <a href="{{route('admin.companies')}}" class="btn btn-primary">Go to companies</a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Employees -- {{$users}}</h5>
          <p class="card-text">Number of employees in the system</p>
          <a href="{{route('admin.users')}}" class="btn btn-primary">Go to Employees</a>
        </div>
      </div>
    </div>
    </div>
  </div>

  @if(auth()->user()->role_id==1)
  <br>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Admin -- {{$admins}}</h5>
            <p class="card-text">Number of Admin in the system</p>
            <a href="{{route('super-admin.admins')}}" class="btn btn-primary">Go to Admin</a>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection