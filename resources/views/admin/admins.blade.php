@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Admins</h3>
        </div>
        <users-component users-type='admins'/>
    </div>
@endsection