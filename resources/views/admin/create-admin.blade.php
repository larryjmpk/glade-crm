@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Create Admin</h3><span class="text-error" style="color: red;">(All fields are required)</span>
        </div>
        <div class="card-body">
            <create-admin-component panel-title="Create Admin" post-action={{route('admin.store-user')}}>
        </div>
    </div>
    
@endsection