<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable
{
    use HasFactory;

    protected $guarded = [];

    public function employees(){
        return $this->hasMany(User::class,'company_id');
    }

    public function admin()
    {
        //return User::find(1);
        return $this->belongsTo(User::class,'user_id');
    }
}
