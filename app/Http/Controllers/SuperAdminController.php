<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\Interfaces\RepositoryInterface;


class SuperAdminController extends Controller
{
    private $user;

    function __construct(RepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create-admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->user->create($request->all());
        return response(['message'=>'Admin created successfully','success'=>true],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit-admin',['id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $id)
    {
        if($this->user->valueExists('email',$request->email,$id->id)){
            return response(['message'=>'Email Already exist','success'=>false],422);
         }
         $this->user->update($id,$request->all());
         return response(['message'=>'User updated successfully','success'=>true],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $id)
    {
        $this->user->delete($id);

        return response(['message'=>"User delete successfully",'success'=>true]);
    }

    /**
     * load Employees resource
     */
    public function all(){
        $users = $this->user->getModel()->where('role_id',2)->paginate(20);
        return response(['users'=>$users,'success'=>true],200);
    }

    public function admins()
    {
        return view('admin.admins');
    }
}
